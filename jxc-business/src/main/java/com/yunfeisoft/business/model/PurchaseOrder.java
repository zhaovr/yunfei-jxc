package com.yunfeisoft.business.model;

import com.applet.base.ServiceModel;
import com.applet.sql.record.TransientField;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * ClassName: PurchaseOrder
 * Description: 采购单信息
 *
 * @Author: Jackie liu
 * Date: 2020-07-23
 */
@Entity
@Table(name = "TT_PURCHASE_ORDER")
public class PurchaseOrder extends ServiceModel implements Serializable {

    /**
     * Field serialVersionUID: 序列号
     */
    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    @Column
    private String orgId;

    /**
     * 供应商id
     */
    @Column
    private String supplierId;

    /**
     * 订单编号
     */
    @Column
    private String code;

    /**
     * 采购日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Column
    private Date purchaseDate;

    /**
     * 总价
     */
    @Column
    private BigDecimal totalAmount;

    /**
     * 备注
     */
    @Column
    private String remark;

    /**
     * 状态(1待入库，2已入库，3待更改)
     */
    @Column
    private Integer status;

    /**
     * 付款状态(1待付款，2已付款，3待付清)
     */
    @Column
    private Integer payStatus;

    /**
     * 已付金额
     */
    @Column
    private BigDecimal payAmount;

    @TransientField
    private String supplierName;
    private List<PurchaseItem> purchaseItemList;
    private List<PaymentRecord> paymentRecordList;
    private PaymentRecord paymentRecord;
    private Supplier supplier;

    /**
     * 待付金额
     * @return
     */
    public BigDecimal getSurplusAmount() {
        return totalAmount.subtract(payAmount);
    }

    public String getStatusStr() {
        return PurchaseOrderStatusEnum.valueOf(status);
    }

    public String getPayStatusStr() {
        return PurchaseOrderPayStatusEnum.valueOf(payStatus);
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public List<PurchaseItem> getPurchaseItemList() {
        return purchaseItemList;
    }

    public void setPurchaseItemList(List<PurchaseItem> purchaseItemList) {
        this.purchaseItemList = purchaseItemList;
    }

    public List<PaymentRecord> getPaymentRecordList() {
        return paymentRecordList;
    }

    public void setPaymentRecordList(List<PaymentRecord> paymentRecordList) {
        this.paymentRecordList = paymentRecordList;
    }

    public PaymentRecord getPaymentRecord() {
        return paymentRecord;
    }

    public void setPaymentRecord(PaymentRecord paymentRecord) {
        this.paymentRecord = paymentRecord;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    /**
     * 状态(1待入库，2已入库，3待更改)
     */
    public enum PurchaseOrderStatusEnum {

        TO_BE_STORAGE(1, "待入库"),
        STORAGED(2, "已入库"),
        TO_BE_CHANGED(3, "待更改");

        private int value;
        private String label;

        private PurchaseOrderStatusEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (PurchaseOrderStatusEnum loop : PurchaseOrderStatusEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }

    /**
     * 付款状态(1待付款，2已付款，3待付清)
     */
    public enum PurchaseOrderPayStatusEnum {

        //TO_BE_PAID(1, "待付款"),
        PAID(2, "已结清"),
        TO_BE_CLEARED(3, "欠款");

        private int value;
        private String label;

        private PurchaseOrderPayStatusEnum(int value, String label) {
            this.value = value;
            this.label = label;
        }

        public static String valueOf(Integer value) {
            if (value == null) {
                return null;
            }
            for (PurchaseOrderPayStatusEnum loop : PurchaseOrderPayStatusEnum.values()) {
                if (value == loop.getValue()) {
                    return loop.getLabel();
                }
            }
            return null;
        }

        public int getValue() {
            return value;
        }

        public String getLabel() {
            return label;
        }
    }
}