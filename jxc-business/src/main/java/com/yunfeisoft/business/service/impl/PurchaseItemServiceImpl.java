package com.yunfeisoft.business.service.impl;

import com.applet.base.BaseServiceImpl;
import com.applet.sql.separation.DataSourceChange;
import com.applet.utils.Page;
import com.yunfeisoft.business.dao.inter.PurchaseItemDao;
import com.yunfeisoft.business.model.PurchaseItem;
import com.yunfeisoft.business.service.inter.PurchaseItemService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * ClassName: PurchaseItemServiceImpl
 * Description: 采购单商品信息service实现
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Service("purchaseItemService")
public class PurchaseItemServiceImpl extends BaseServiceImpl<PurchaseItem, String, PurchaseItemDao> implements PurchaseItemService {

    @Override
    @DataSourceChange(slave = true)
    public Page<PurchaseItem> queryPage(Map<String, Object> params) {
        return getDao().queryPage(params);
    }

    @Override
    public List<PurchaseItem> queryByPurchaseOrderId(String purchaseOrderId) {
        return getDao().queryByPurchaseOrderId(purchaseOrderId);
    }

}