package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.Warehouse;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: WarehouseService
 * Description: 仓库信息service接口
 * Author: Jackie liu
 * Date: 2020-08-04
 */
public interface WarehouseService extends BaseService<Warehouse, String> {

    public Page<Warehouse> queryPage(Map<String, Object> params);

    public List<Warehouse> queryList(Map<String, Object> params);

    public int modifyDefault(String id, String orgId);

    public Warehouse loadDefault(String orgId);

    public boolean isDupName(String orgId, String id, String name);
}