package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.yunfeisoft.business.model.CodeBuilder;
import com.yunfeisoft.business.service.inter.CodeBuilderService;
import com.applet.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: CodeBuilderController
 * Description: 编码生成记录Controller
 * Author: Jackie liu
 * Date: 2020-07-23
 */
@Controller
public class CodeBuilderController extends BaseController {

    @Autowired
    private CodeBuilderService codeBuilderService;

    /**
     * 添加编码生成记录
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/codeBuilder/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(CodeBuilder record, HttpServletRequest request, HttpServletResponse response) {
        codeBuilderService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改编码生成记录
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/codeBuilder/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(CodeBuilder record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        codeBuilderService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询编码生成记录
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/codeBuilder/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        CodeBuilder record = codeBuilderService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询编码生成记录
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/codeBuilder/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        Page<CodeBuilder> page = codeBuilderService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除编码生成记录
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/codeBuilder/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        codeBuilderService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
