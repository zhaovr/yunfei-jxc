package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.yunfeisoft.business.model.WarehouseUser;
import com.yunfeisoft.business.service.inter.WarehouseUserService;
import com.applet.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: WarehouseUserController
 * Description: 仓库用户信息Controller
 * Author: Jackie liu
 * Date: 2020-08-04
 */
@Controller
public class WarehouseUserController extends BaseController {

    @Autowired
    private WarehouseUserService warehouseUserService;

    /**
     * 添加仓库用户信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseUser/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(WarehouseUser record, HttpServletRequest request, HttpServletResponse response) {
        warehouseUserService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改仓库用户信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseUser/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(WarehouseUser record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        warehouseUserService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询仓库用户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseUser/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        WarehouseUser record = warehouseUserService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询仓库用户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseUser/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        Page<WarehouseUser> page = warehouseUserService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除仓库用户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/warehouseUser/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        warehouseUserService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}
