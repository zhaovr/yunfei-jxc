<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑收付款信息</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">日期<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.occurDate" id="occurDate" class="layui-input" style="cursor:pointer;" readonly/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">类型<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<select v-model="record.type" class="layui-input">
						<option value="">请选择</option>
						<option value="1">收款</option>
						<option value="2">付款</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">项目<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.name" placeholder="请输入类目" class="layui-input" style="width:200px;display:inline-block;"/>
					<button type="button" class="layui-btn input-btn layui-btn-normal" @click="showProductParamsDialog()">类目</button>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">金额<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.amount" placeholder="请输入金额" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea v-model="record.remark" placeholder="请输入备注" class="layui-textarea"></textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record : {
				id:'${params.id!}',
				occurDate:'${.now?string('yyyy-MM-dd')}',
				type:'',
				name:'',
				amount:'',
				remark:'',
			},
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		methods: {
			init:function(){
				var that = this;
				laydate.render({elem: '#occurDate', type:'date', done:function (value) {
						that.record.occurDate = value;
					}});
			},
			loadData: function () {
				if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/paymentReceived/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
				});
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/paymentReceived/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
			showProductParamsDialog: function () {
				var type = this.record.type;
				if (!type) {
					$.message("请先选择类型");
					return;
				}

				type = type == '1' ? 3 : 4;
				var url = "${params.contextPath!}/view/business/productParams/productParams_dialog.htm?type=" + type;
				DialogManager.open({url: url, width: '90%', height: '90%', title: '选择参数'});
			},
			productParamsSelectCallBack: function (row) {
				this.record.name = row.name;
				DialogManager.closeAll();
			},
		}
	});
</script>
</body>

</html>
